# Pack
Extremely in progress

### Disclaimer
This has hardly been worked on and is essentially useless at its current state. Please don't use this unless you plan on developing it.

### TODO
[\*] Parse cmdline options well, including parsing the package name from the package repo

[ ] Correctly download from several repos

[\*] Unpack tarballs of various types

[ ] Correctly install binaries in system

[ ] More, probably

### Building and Setup
A build and setup script has been included as `scripts/build`, but as it is written in Lua, you may wish to setup yourself. To do that, 
1. Run `cargo build --release`
2. Run `scripts/setup.sh`
3. Run `scripts/mkenv.sh arch_core` to setup the arch core repo
4. Add /usr/pack/bin to path

### Usage
Firstly, to setup your environment, run scripts/arch.sh. The syntax of the command is as follows:

`sudo pack {url}::{repo type}`

This will add the tarball found at {url} to /usr/pack/repo/{repo type}/tarballs.
