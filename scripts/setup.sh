#!/bin/bash

if [ ! -d "/usr/pack" ]; then
	printf "Creating /usr/pack/\n"
	sudo mkdir /usr/pack
fi

if [ ! -d "/usr/pack/repo" ]; then
	printf "Creating /usr/pack/repo\n"
	sudo mkdir /usr/pack/repo
fi

if [ ! -d "/usr/pack/pack.toml" ]; then
	printf "Creating /usr/pack/pack.toml\n"
	sudo touch /usr/pack/pack.toml
fi

if [ ! -d "/usr/pack/bin/" ]; then
	printf "Creating /usr/pack/bin/\n"
	sudo mkdir /usr/pack/bin
fi
