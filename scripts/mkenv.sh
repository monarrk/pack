#!/bin/bash

PREFIX="/usr/pack/repo"

if [[ -z $1 ]]; then
	printf "Please enter a repo name!\n"
	exit 1
fi

if [ ! -d "$PREFIX/$1" ]; then
	printf "Creating $PREFIX/$1\n"
	sudo mkdir "$PREFIX/$1"
fi

if [ ! -d "$PREFIX/$1/tarballs" ]; then
	printf "Creating $PREFIX/$1/tarballs\n"
	sudo mkdir "$PREFIX/$1/tarballs"
fi
