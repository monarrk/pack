use std::result::Result;
use std::path::Path;

use reqwest::Client;

mod arch;

fn err_and_exit(msg: String, code: i32) {
    println!("ERROR!: {}", msg);
    std::process::exit(code);
}

fn main() -> Result<(), std::io::Error> {
    let client = Client::new();

    let args: Vec<_> = std::env::args().collect();
    if args.len() < 2 {
        println!("[PACK]: Please enter a package name...");
        std::process::exit(1);
    }

    let split = args[1].as_str().split("::");
    let vec: Vec<&str> = split.collect();
    if vec.len() < 2 {
       err_and_exit("Please enter a repo to download from!".to_string(), 0); 
    }

    println!("[PACK]: Downloading {}", vec[0]);

    if !Path::new(format!("/usr/pack/repo/{}", vec[1]).as_str()).exists() {
        err_and_exit(format!("Path /usr/pack/repo/{} does not exist. Please setup your environment.", vec[1]), 1);
    }

    let full_path: Vec<&str> = vec[0].split("/").collect();
    let pkgname = full_path[full_path.len() - 1];
    let repo = match vec[1] { // get repo url based on command line argument
        "arch_core" => "http://mirrors.advancedhosters.com/archlinux/core/os/x86_64/",
        "arch_extra" => "http://mirrors.advancedhosters.com/archlinux/extra/os/x86_64/",
        "arch_community" => "http://mirrors.advancedhosters.com/archlinux/community/os/x86_64/",
        "void" => "http://mirror.clarkson.edu/voidlinux/current/x86_64-repodata",
        _ => {
            println!("ERROR!: Repo {} is not supported...", vec[1]);
            std::process::exit(0);
        },
    };
   
    arch::get_arch_pkg(repo, pkgname, vec, client);

    Ok(())
} 
