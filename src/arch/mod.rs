use std::fs::File;
use std::process::Command;

use reqwest::Client;

use crate::err_and_exit;

pub fn get_arch_pkg(repo: &str, pkgname: &str, vec: Vec<&str>, client: Client) {
    print!("[PACK]: Making request...");
    let url = reqwest::Url::parse(&(repo.to_string() + pkgname)).expect("Failed to parse URL");
    let mut resp = match client.get(url).send() {
        Ok(a) => a,
        Err(e) => {
            println!("ERROR!: Couldn't get package from url. Are you connected to the internet?\n\n{}", e);
            std::process::exit(0);
        },
    };
    
    if resp.status().is_success() {
        println!("OK");

        let mut file = match File::create(format!("/usr/pack/repo/{}/tarballs/{}", vec[1], pkgname)) {
            Ok(res) => res,
            Err(e) => {
                println!("Failed to create file...did you setup you environment? Do you have permission to create files?\n\n{}", e);
                std::process::exit(0);
            },
        };

        resp.copy_to(&mut file).expect("failed to write file");
    } else {
        err_and_exit(format!("Request was unsuccessful...did you request from the correct repo?\nStatus is: \"{}\"", resp.status()), 0)
    }

    let spltdirname: Vec<&str> = pkgname.split(".").collect();
    let dirname = spltdirname[0];

    print!("[PACK]: Creating package dir...");
    match std::fs::create_dir(format!("/usr/pack/repo/{}/{}", vec[1], dirname)) {
        Ok(_) => (),
        Err(e) => err_and_exit(format!("Failed to create directory! Remove the directory if it already exists. Got error \"{}\"", e), 0),
    }
    println!("OK");

    print!("[PACK]: Executing tar...");
    Command::new("tar")
        .arg(format!("-C/usr/pack/repo/{}/{}", vec[1], dirname))
        .arg("-xf")
        .arg(format!("/usr/pack/repo/{}/tarballs/{}", vec[1], pkgname))
        .output()
        .expect("Failed to execute tar");
    println!("OK");

    let paths = std::fs::read_dir(format!("/usr/pack/repo/{}/{}/usr/bin/", vec[1], dirname)).expect("Failed to read dir...");
    println!("[PACK]: Creating symlinks");
    for file in paths {
        let pth = &file.expect("Failed to read file").path();
        let strpth = pth.to_str().expect("Failed to convert to str...");
        println!("\tAdding file {} to /usr/pack/bin/", strpth);
        
        let a: Vec<&str> = strpth.split("/").collect();
        let name = a[a.len() - 1];
        std::os::unix::fs::symlink(pth, format!("/usr/pack/bin/{}", name)).expect("Failed to symlink...");
    }
}
